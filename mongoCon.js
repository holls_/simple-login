const mongoose = require('mongoose')

mongoose.connect(' mongodb://127.0.0.1:27017/test?gssapiServiceName=mongodb', { useNewUrlParser: true });

const conn = mongoose.connection

const userSchema = new mongoose.Schema({
    username: { type: String },
    email: { type: String },
    password: { type: String }
})

const User = mongoose.model('User', userSchema)
conn.on('error', (err) => {
    console.log('Algo no mongoose deu erro')
    console.error(err)
})

conn.once('open', (err) => {
    console.log(`Estamos ligados à base de dados do Mongo (${conn.db.collection('users').collectionName})`)
})

module.exports = {
    conn: conn,
    User: User
};