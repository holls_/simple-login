const express = require('express')
require('dotenv').config()
const morgan = require('morgan')
const { conn, User } = require('./mongoCon')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const axios = require('axios')
const path = require('path')
const exphbs = require('express-handlebars')

const app = express()

const port = process.env.PORT || 5000
const app_dir = path.join(__dirname, 'app')

console.log(User)

app.use(morgan('dev'))
app.use('/css', express.static(app_dir))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
/**
 * Configurações para as Views
 */
app.set('views', 'app/views');
app.engine('.hbs', exphbs({
    layoutsDir: 'app/views/mainLayout',
    defaultLayout: 'index',
    extname: '.hbs'
}))
app.set('view engine', '.hbs')

app.get('/', (req, res) => {
    //Isto pode passar também para um html(.hbs) na pasta view que depois dá render
    res.send(`<h1 style="color: red;">Simple Login and Session Management</h1>
    <ul style="list-style-type: none">
        <li style="text-decoration: line-through">Criar pasta views e os templates (handlebars)</li>
        <li>Usar os partials do handlebars para mostrar em todas as páginas 
        todos os users da base de dados (Este html também podia ser uma partial)</li>
        <li>Registar User</li>
        <li>Autenticar o user com passport ou JWT...</li>
        <li>Fazer login e depois Logout</li>
        <li>Gerir as Sessões e controlar o acesso do user</li>
        <li>Organizar o server.js por rotas</li>
        <li>Lindo lindo era mandar mails</li>    
    </ul>`) //style="text-decoration: line-through"
})


/**
 * Get's 
 */
app.get('/login', function (req, res) {
    res.render('login', {
        title: 'Login',
        greetings: 'Hello madafakaaaaa!!!!!'
    })
})

app.get('/signup', function (req, res) {
    res.render('signup', {
        title: 'Sign Up'
    })
})

app.get('/showall', function () {
    /**
     * Em modo MVC esta função de procura estaria noutro 
     * .js que seria require em cima
     */
    User.find({}, (err, data) => {
        console.log(data)
    })

    res.render('showAll')
})

/**
 * Post's
 */
app.post('/registeruser', function (req, res) {

})

app.listen(port, () => {
    console.log(`O servidor para fazer login e gerir sessões está a correr na porta ${port}`)
})